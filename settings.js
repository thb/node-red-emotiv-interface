module.exports = {
    uiPort: process.env.PORT || 1880,
    mqttReconnectTime: 15000,
    serialReconnectTime: 15000,
    debugMaxLength: 1000,
    flowFilePretty: true,
    disableEditor: true,

    functionGlobalContext: {},
    functionExternalModules: false,
    exportGlobalContextKeys: false,

    //paletteCategories: ['subflows', 'common', 'function', 'network', 'sequence', 'parser', 'storage'],

    logging: {
        console: {
            level: "info",
            metrics: false,
            audit: false
        }
    },

    externalModules: {},

    editorTheme: {
        projects: {
            enabled: false,
            workflow: {
                mode: "manual"
            }
        }
    }
}
